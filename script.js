/*
Наведіть кілька прикладів, коли доречно використовувати в коді конструкцію try...catch.

Під час взаємодії з базою даних можуть виникати помилки, наприклад, неправильний запит або проблеми з авторизацією.
try...catch дозволяє перехопити помилку і виконати необхідні дії, наприклад, повідомити користувачеві про проблему
або зберегти стан даних перед закриттям з'єднання.
Під час обчислення також можуть виникати помилки, наприклад, ділення на нуль.
try...catch перехоплює помилку і, наприклад, присвоює дефолтне значення або повідомляє користувачеві про помилку.
*/

const books = [
    {
        author: "Люсі Фолі",
        name: "Список запрошених",
        price: 70
    },
    {
        author: "Сюзанна Кларк",
        name: "Джонатан Стрейндж і м-р Норрелл",
    },
    {
        name: "Дизайн. Книга для недизайнерів.",
        price: 70
    },
    {
        author: "Алан Мур",
        name: "Неономікон",
        price: 70
    },
    {
        author: "Террі Пратчетт",
        name: "Рухомі картинки",
        price: 40
    },
    {
        author: "Анґус Гайленд",
        name: "Коти в мистецтві",
    }
];

/*class Books {
    constructor() {
        this.mainDiv = document.createElement("div");
        this.unorderList = document.createElement("ul");
        this.listItems = document.createElement("li");
    }
    createElements() {
        this.mainDiv.setAttribute("id", "root");

        this.mainDiv.append(this.unorderList);
        this.mainDiv.append(this.listItems);
    }
}*/

function isValid(book) {
    return "author" in book && 'name' in book && 'price' in book;
}
function render(books) {
    this.mainDiv = document.createElement("div");
    this.mainDiv.setAttribute("id", "root");
    document.body.appendChild(this.mainDiv);

    this.unorderList = document.createElement("ul");
    this.mainDiv.appendChild(this.unorderList);

    books.forEach((book, index) => {
        try {
            if (isValid(book)) {
                this.listItem = document.createElement("li");
                this.listItem.textContent = `${book.author}, ${book.name}, Ціна: ${book.price}`;
                this.unorderList.appendChild(this.listItem);
            } else {
                throw new Error(`Помилка в об'єкті №${index + 1}: Не всі необхідні властивості присутні.`);
            }
        } catch (err) {
            console.error(err);
        }
    });
}

render(books);